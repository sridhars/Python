

text_results = ['hello', 'foo' , 'hi' , 'good' , 'this' , 'hi', 'kl', 'ab', 'fff' ]
scores = [4,2,4,5,1,4, ]
reverse = True

def min(value1, value2):

	return value1 if value1 < value2 else value2

def combine_list(list1, list2, rev):

	list_length = min( len(list1), len(list2) )
		
	return sorted([(list1[i], list2[i]) for i in xrange(list_length)], reverse=rev)



print combine_list(scores, text_results, reverse)
